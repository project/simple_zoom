CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Simple Zoom module allows you to open the image in a popup. The module
creates an image formatter and a CKEditor plugin for opening the image fields
and inline images in a popup using a simple jquery zoom plugin.

 * For a full description of the module visit:
   https://www.drupal.org/project/simple_zoom

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/simple_zoom


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Simple Zoom module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

 * Navigate to Administration > Extend and enable the module.

Instructions For Opening Image Fields In Popup:

 * Navigate to Administration > Structure > Content types > Content Type > Manage
   Display and change the format of the image field to "Zoom image".

Instructions For Opening Inline Images In Popup:

 * Enable the zoom_inline module. This automatically opens the inline images,
   uploaded through CK Editor in popup.


MAINTAINERS
-----------

 * sravya challa (sravya_12) - https://www.drupal.org/u/sravya_12

Supporting organization:

 * Valuebound - https://www.drupal.org/valuebound
